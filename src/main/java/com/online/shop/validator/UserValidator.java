package com.online.shop.validator;
import com.online.shop.dto.UserDto;
import com.online.shop.entities.User;
import com.online.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserValidator {

    @Autowired
    private UserRepository userRepository;
    public void validate(UserDto userDto, BindingResult bindingResult) {
        //de implementat

//        //Regular Expression
//        String regex = "^(.+)@(.+)$";
//        //Compile regular expression to get the pattern
//        Pattern pattern = Pattern.compile(regex);
//        //Create instance of matcher
//        Matcher matcher = pattern.matcher(userDto.getEmail());
//        if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
//            FieldError fieldError = new FieldError("userDto", "confirmPassword", "Password does not match!");
//            bindingResult.addError(fieldError);
//        }
        // emailul este unic?
        Optional<User> optionalUser = userRepository.findByEmail(userDto.getEmail());
        if(optionalUser.isPresent()){
            FieldError fieldError = new FieldError("userDto", "email", "Email is already in use!");
            bindingResult.addError(fieldError);
        }

    }

}

